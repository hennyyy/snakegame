PImage img1, img2, img3, img4;

int grid = 20; // mengatur besar ukuran grid layar
PVector food, obstacle; 
int speed = 30; // kecepatan gerak snake
boolean dead = true; 
int highscore = 0;
Snake snake;
import processing.sound.*;
SoundFile file1, file2, file3;

AudioIn in;

void setup() {
  img1 = loadImage("bg.jpg");
  img2 = loadImage("snake1.png");
  img3 = loadImage("apple2.png");
  img4 = loadImage("apple0.png");
  fullScreen();
  //size(1280, 720); // ukuran layar
  snake = new Snake(); // membuat objek baru
  food = new PVector(); // membuat objek food
  obstacle = new PVector();
  newFood();
  newObstacle();
  file1 = new SoundFile(this, "audio.wav");
  file2 = new SoundFile(this, "theme.mp3");
  file3 = new SoundFile(this, "over.mp3");
  file2.loop();
  
  
}

void draw() {

  if (!dead) {
    file3.stop();
    background(85,79,19);
    
    if (frameCount % speed == 0) {
      snake.update();
    }
    snake.show();
    snake.eat();
    snake.obstacle();
    
    textAlign(LEFT);
    textSize(15);
    fill(255);
    text("Press 'p' to Pause and 'r' to Resume\nHigh Score : " + highscore + "\nScore :" + snake.len, 10, 20);
    
    //fill(255, 9, 0); // warna makanan
    image(img3, food.x, food.y, 1.25*grid, 1.25*grid);
    image(img4, obstacle.x, obstacle.y, 1.25*grid, 1.25*grid);

    
    
    
  } else {
    image(img1,0,0,width,height);
    //background(img1);
    fill(0);
    textSize(30);
    textAlign(CENTER);
    text("\nScore: " + snake.len, width/2+10, height/2);
    text("\nHighscore: " + highscore, width/2+10, height/2+40);
  }
}

void newFood() {
  food.x = floor(random(width)); // posisi makanan diletakan di posisi x dan y yang dibuat random
  food.y = floor(random(height));
  food.x = floor(food.x/grid) * grid;
  food.y = floor(food.y/grid) * grid;
}

void newObstacle() {
  obstacle.x = floor(random(width)); // posisi makanan diletakan di posisi x dan y yang dibuat random
  obstacle.y = floor(random(height));
  obstacle.x = floor(obstacle.x/grid) * grid; // posisi makanan diletakan di posisi x dan y yang dibuat random
  obstacle.y = floor(obstacle.y/grid) * grid;
}

void mousePressed() {
  if (dead) {
    file2.loop();
    snake = new Snake();
    newFood();
    newObstacle();
    speed =10;
    dead = false;
  }
}
